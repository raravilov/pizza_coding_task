FROM python:3.6-alpine
ENV PYTHONPATH=${PYTHONPATH}:/app
RUN apk add --no-cache gcc make libc-dev postgresql-dev
RUN apk add --no-cache python3-dev build-base linux-headers pcre-dev curl jpeg-dev zlib-dev
COPY pizza_project /app
ADD ./requirements*.txt /app/
WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt