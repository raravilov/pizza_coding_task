from django.db import transaction
from rest_framework import serializers

from pizza_app.models import Order, Pizza, PizzaCount, Status, Size, Count


class PizzaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pizza
        fields = [
            'name'
        ]


class CountSerializer(serializers.ModelSerializer):

    class Meta:
        model = Count
        fields = [
            'name'
        ]

class PizzaCountSerializer(serializers.ModelSerializer):
    pizza = serializers.SerializerMethodField()

    class Meta:
        model = PizzaCount
        fields = [
            'id',
            'pizza',
            'count',
            'order',
            'size'
        ]

    def get_pizza(self, obj):
        pizza = obj.pizzas.all()
        return PizzaSerializer(pizza, many=True).data


class OrderCreateSerializer(serializers.Serializer):
    date_publish = serializers.DateField(read_only=True)
    pizza_list = serializers.ListField(required=True)
    customer = serializers.CharField(required=True)

    def create(self, validated_data):
        order = Order.objects.create(customer=validated_data['customer'], status=Status.objects.get(id=1))

        with transaction.atomic():
            pizza_list = [[Pizza.objects.get(id=item[0]), Count.objects.get(id=item[1]), Size.objects.get(id=item[2])]
                          for item in validated_data['pizza_list']]

            pizza_count = [PizzaCount(pizza=pizza, order=order, count=count, size=size)
                           for pizza, count, size in pizza_list]
            PizzaCount.objects.bulk_create(pizza_count, ignore_conflicts=True)

        return order


class OrderSerializer(serializers.ModelSerializer):
    pizza = PizzaSerializer(many=True)
    count = CountSerializer(many=True)

    class Meta:
        model = Order
        fields = [
            'id',
            'customer',
            'date_publish',
            'status',
            'pizza',
            'count'
        ]


class OrderUpdateSerializer(serializers.Serializer):
    pizza_list = serializers.ListField(write_only=True)

    def validate(self, attrs):
        if self.instance.status.name == 2:
            raise serializers.ValidationError('You cant change this status')

        return attrs

    def update(self, instance, validated_data):
        PizzaCount.objects.filter(order=instance).delete()

        with transaction.atomic():
            pizza_list = [[Pizza.objects.get(id=item[0]), Count.objects.get(id=item[1]), Size.objects.get(id=item[2])]
                          for item in validated_data['pizza_list']]
            pizza_count = [PizzaCount(pizza=pizza, order=instance, count=count, size=size)
                           for pizza, count, size in pizza_list]
            PizzaCount.objects.bulk_create(pizza_count, ignore_conflicts=True)

        return instance

