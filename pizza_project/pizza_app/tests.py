from django.urls import reverse
from rest_framework.test import APITestCase
from django.test import Client
from rest_framework.utils import json


class OrderTests(APITestCase):
    fixtures = ('test_order',)

    def setUp(self):
        self.client = Client()
        self.check_dict = {"customer": "test", "pizza_list": [[1,1,1]]}

    def test_orders_create_201(self):
        response = self.client.post(reverse('Orders-list'),
                                    json.dumps(self.check_dict),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201)

    def test_orders_list_200(self):
        response = self.client.get(reverse('Orders-list'))
        self.assertEqual(response.status_code, 200)
