
from rest_framework import mixins, viewsets, status
from rest_framework.response import Response

from pizza_app.models import Order
from pizza_app.serializers import OrderCreateSerializer, OrderSerializer, OrderUpdateSerializer


class OrderViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                        mixins.DestroyModelMixin, mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_serializer_class(self):
        if self.action == 'create':
            return OrderCreateSerializer

        return self.serializer_class

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):

        return super().retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):

        order = self.get_object()

        serializer = OrderUpdateSerializer(instance=order, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(OrderSerializer(instance=order).data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)