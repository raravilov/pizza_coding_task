from django.db import models


class Status(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Size(models.Model):
    name = models.CharField(max_length=3, unique=True)

    def __str__(self):
        return self.name


class Count(models.Model):
    name = models.IntegerField()


class Pizza(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    customer = models.TextField(null=True, blank=True)
    date_publish = models.DateField(auto_now_add=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    pizza = models.ManyToManyField(Pizza, through='PizzaCount', related_name='orders')
    count = models.ManyToManyField(Count, through='PizzaCount', related_name='orders')


class PizzaCount(models.Model):
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    count = models.ForeignKey(Count, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    size = models.ForeignKey(Size, on_delete=models.CASCADE)
