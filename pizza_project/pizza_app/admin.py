from django.contrib import admin
from pizza_app.models import Pizza, Status, Order, PizzaCount, Size, Count


@admin.register(Pizza)
class PizzaAdmin(admin.ModelAdmin):
    pass


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(PizzaCount)
class PizzaCountAdmin(admin.ModelAdmin):
    pass


@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    pass


@admin.register(Count)
class CountAdmin(admin.ModelAdmin):
    pass