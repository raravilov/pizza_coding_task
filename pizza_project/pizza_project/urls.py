from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from pizza_app import views





router = routers.DefaultRouter()

router.register(r'orders', views.OrderViewSet, 'Orders')


urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('admin/', admin.site.urls),
]


